<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'APIController@login');
Route::post('register', 'APIController@register');

Route::group(['middleware' => 'auth.jwt'], function () {
    
    Route::apiResource('programms', 'ProgrammController');
    // Route::get('logout', 'APIController@logout');
    // Route::post('add', 'ProgrammController@store');
    // Route::get('programms', 'ProgrammController@index');

   
});