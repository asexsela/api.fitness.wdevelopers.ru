<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6|max:10'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Заполните поле Имя',
            'name.string' => 'Только буквы',
            'email.required' => 'Заполните поле Почта',
            'email.email' => 'Введите email',
            'email.unique' => 'Пользователь с таким email уже зарегистрирован',
            'password.required' => 'Заполните поле Пароль',
            'password.min' => 'Минимум 6 символов',
            'password.max' => 'Максимум 10 символов',
            'password.confirmed' => 'Пароли не совпадают',
        ];
    }
}
