<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Programm;
use Illuminate\Http\Request;

class ProgrammController extends Controller
{


    /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
        // $this->user = JWTAuth::getToken();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programms = $this->user->programms()->get(['title', 'description'])->toArray();

        return $programms;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);
    
        $task = new Programm();
        $task->title = $request->title;
        $task->description = $request->description;
    
        if ($this->user->programms()->save($task))
            return response()->json([
                'success' => true,
                'task' => $task
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be added.'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Programm  $programm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programm = $this->user->programms()->find($id);

        if (!$programm) {
            return response()->json([
                'success' => false,
                'message' => 'Извините программа с id ' . $id . ' не найдена.'
            ], 400);
        }

        return $programm;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Programm  $programm
     * @return \Illuminate\Http\Response
     */
    public function edit(Programm $programm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Programm  $programm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Programm $programm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Programm  $programm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Programm $programm)
    {
        //
    }
}
